import { MongoClient } from 'mongodb'
import { mongoUrl } from '../env'
export const cli = new MongoClient(mongoUrl, { useUnifiedTopology: true })
export const DB = cli.connect().then((mongoClient) => mongoClient.db())
