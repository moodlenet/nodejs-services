import { MongoNode, MongoRelation } from '../gql-graph/mongo/mappers'
import { GqlType, GqlNode, GqlRelation } from '../gql-graph/types'
import { DB } from './'

// export const collection = async <E extends GqlType>(name: E['__typename']) =>
//   (await DB).collection<
//     E extends GqlNode ? MongoNode<E> : E extends GqlRelation ? MongoRelation<E> : never
//   >(name)

export const graphCollection = async <E extends GqlType>() =>
  (await DB).collection<
    E extends GqlNode ? MongoNode<E> : E extends GqlRelation ? MongoRelation<E> : never
  >('Graph')
