import { logLevel } from '../env'
import winston from 'winston'
const { format } = winston
const { combine, label, json } = format

export const simpleLogger = winston.createLogger({
  format: combine(label({ label: 'simple' }), json()),
  transports: [
    new winston.transports.Console({ level: logLevel }),
    //new winston.transports.File({ filename: 'somefile.log' })
  ],
})
