import { httpPort } from './env'
import { schema } from './gql'
import { start } from './http'
import * as logger from './util/logger'

start({
  httpPort,
  schema,
}).addListener('listening', () => {
  logger.simpleLogger.info(`server started on port ${httpPort}`)
})
