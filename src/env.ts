import { readFileSync } from 'fs'

const { MONGO_URL, HTTP_PORT, LOG_LEVEL } = process.env

export const typeDefs = readFileSync(`${__dirname}/../graphql/schema.graphql`, 'utf-8')

export const mongoUrl = MONGO_URL || 'mongodb://localhost:27017/mn'
export const httpPort = Number(HTTP_PORT || 4001)
export const logLevel = LOG_LEVEL || 'silly'
