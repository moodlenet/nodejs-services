import { defaultGraphFieldResolver } from '../../../gql-graph/graphqlResolver'
import { QueryResolvers } from '../../types'

export const Query: QueryResolvers = {
  graph: defaultGraphFieldResolver(),
}
