import { Resolvers } from '../types'
import { Mutation } from './mutation'
import { Query } from './query'
import { Types } from './types'

export const resolvers: Resolvers = {
  Query,
  Mutation,
  ...Types,
} as Resolvers
