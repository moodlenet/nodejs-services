import { ObjectID } from 'mongodb'
import { toMongoRelation } from '../../../gql-graph/mongo/mappers'
import { graphCollection } from '../../../mongo/collection'
import { Knows, MutationResolvers } from '../../types'

export const createKnows: MutationResolvers['createKnows'] = async (_parent, args) => {
  const newKnows = toMongoRelation<Knows>(
    {
      __typename: 'Knows',
    },
    new ObjectID(args.to),
    new ObjectID(args.from)
  )
  const c = await graphCollection<Knows>()
  await c.insertOne(newKnows)
  return null //{ ...newKnows, _id: newKnows._id.toHexString(), _obj: [], _subj: [] }
}
