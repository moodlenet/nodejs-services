import { MutationResolvers } from '../../types'
import { createUser } from './createUser'
import { createFollows } from './createFollows'
import { createKnows } from './createKnows'

export const Mutation: MutationResolvers = {
  createUser,
  createFollows,
  createKnows,
}
