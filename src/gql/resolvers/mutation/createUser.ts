import { graphCollection } from '../../../mongo/collection'
import { toMongoNode } from '../../../gql-graph/mongo/mappers'
import { MutationResolvers, User } from '../../types'

export const createUser: MutationResolvers['createUser'] = async (
  _parent,
  args /* , context, info */
) => {
  const newUser = toMongoNode<User>({
    __typename: 'User',
    username: args.user.username,
  })
  const c = await graphCollection<User>()
  await c.insertOne(newUser)
  return null //{ ...newUser, _id: newUser._id.toHexString(), _rel: [] }
}
