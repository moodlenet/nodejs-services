import { ObjectID } from 'mongodb'
import { toMongoRelation } from '../../../gql-graph/mongo/mappers'
import { graphCollection } from '../../../mongo/collection'
import { Follows, MutationResolvers } from '../../types'

export const createFollows: MutationResolvers['createFollows'] = async (_parent, args) => {
  const newFollows = toMongoRelation<Follows>(
    {
      __typename: 'Follows',
    },
    new ObjectID(args.to),
    new ObjectID(args.from)
  )
  const c = await graphCollection<Follows>()
  await c.insertOne(newFollows)
  return null //{ ...newFollows, _id: newFollows._id.toHexString(), _obj: [], _subj: [] }
}
