import { defaultGraphFieldResolver } from '../../../gql-graph/graphqlResolver'
export const Types = {
  User: {
    _id: defaultGraphFieldResolver(),
    _rel: defaultGraphFieldResolver(),
    username: defaultGraphFieldResolver(),
  },
  Knows: {
    _id: defaultGraphFieldResolver(),
    _subj: defaultGraphFieldResolver(),
    _obj: defaultGraphFieldResolver(),
  },
  Follows: {
    _id: defaultGraphFieldResolver(),
    _subj: defaultGraphFieldResolver(),
    _obj: defaultGraphFieldResolver(),
  },
}
