import { makeExecutableSchema } from 'apollo-server-express'
import { typeDefs } from '../env'
import { applyMiddleware } from 'graphql-middleware'
import { resolvers } from './resolvers'
import { directiveResolvers } from './directives'
import { middlewares } from './middlewares'
import { GraphQuery } from '../gql-graph/types'

export type Context = {
  $graph?: GraphQuery
}
export type RootValue = {}

const _schema = makeExecutableSchema<Context>({
  typeDefs: [typeDefs],
  resolvers: resolvers as any,
  directiveResolvers,
})

export const schema = applyMiddleware(_schema, ...middlewares)
