import { GraphQLResolveInfo } from 'graphql';
import { Context, RootValue } from './';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Glyph = {
  _id: Scalars['ID'];
};

export type StringMatchInput = {
  _eq: Maybe<Scalars['String']>;
  _gt: Maybe<Scalars['String']>;
  _lt: Maybe<Scalars['String']>;
};

export type PageInput = {
  limit: Maybe<Scalars['Int']>;
  after: Maybe<Scalars['ID']>;
};

export type KnowsSubject = User;

export type KnowsObject = User;

export type KnowsSubjectQueryInput = {
  User: Maybe<UserQueryInput>;
};

export type KnowsObjectQueryInput = {
  User: Maybe<UserQueryInput>;
};

export type KnowsQueryInput = {
  _and: Maybe<Array<KnowsQueryInput>>;
  _or: Maybe<Array<KnowsQueryInput>>;
  _id: Maybe<Array<Scalars['ID']>>;
};

export type Knows = Glyph & {
  __typename: 'Knows';
  _id: Scalars['ID'];
  _subj: Array<KnowsSubject>;
  _obj: Array<KnowsObject>;
};


export type Knows_SubjArgs = {
  query: Maybe<KnowsSubjectQueryInput>;
  page: Maybe<PageInput>;
};


export type Knows_ObjArgs = {
  query: Maybe<KnowsObjectQueryInput>;
  page: Maybe<PageInput>;
};

export type FollowsSubject = User;

export type FollowsObject = User;

export type FollowObjectQueryInput = {
  User: Maybe<UserQueryInput>;
};

export type FollowSubjectQueryInput = {
  User: Maybe<UserQueryInput>;
};

export type FollowsQueryInput = {
  _and: Maybe<Array<FollowsQueryInput>>;
  _or: Maybe<Array<FollowsQueryInput>>;
  _id: Maybe<Array<Scalars['ID']>>;
};

export type Follows = Glyph & {
  __typename: 'Follows';
  _id: Scalars['ID'];
  _subj: Array<FollowsSubject>;
  _obj: Array<FollowsObject>;
};


export type Follows_SubjArgs = {
  query: Maybe<FollowSubjectQueryInput>;
  page: Maybe<PageInput>;
};


export type Follows_ObjArgs = {
  query: Maybe<FollowObjectQueryInput>;
  page: Maybe<PageInput>;
};

export type UserQueryInput = {
  _and: Maybe<Array<UserQueryInput>>;
  _or: Maybe<Array<UserQueryInput>>;
  _id: Maybe<Array<Scalars['ID']>>;
  username: Maybe<StringMatchInput>;
};

export type UserRelationQueryInput = {
  Follows: Maybe<FollowsQueryInput>;
  Knows: Maybe<KnowsQueryInput>;
};

export type UserRelation = Knows | Follows;

export type User = Glyph & {
  __typename: 'User';
  _id: Scalars['ID'];
  _rel: Array<UserRelation>;
  username: Scalars['String'];
};


export type User_RelArgs = {
  query: Maybe<UserRelationQueryInput>;
  page: Maybe<PageInput>;
};

export type GraphQueryInput = {
  Knows: Maybe<KnowsQueryInput>;
  Follows: Maybe<FollowsQueryInput>;
  User: Maybe<UserQueryInput>;
};

export type Query = {
  __typename: 'Query';
  graph: Array<Glyph>;
};


export type QueryGraphArgs = {
  query: Maybe<GraphQueryInput>;
  page: Maybe<PageInput>;
};

export type CreateUserInput = {
  username: Scalars['String'];
};

export type Mutation = {
  __typename: 'Mutation';
  createUser: Maybe<User>;
  createKnows: Maybe<Knows>;
  createFollows: Maybe<Follows>;
};


export type MutationCreateUserArgs = {
  user: CreateUserInput;
};


export type MutationCreateKnowsArgs = {
  from: Scalars['ID'];
  to: Scalars['ID'];
};


export type MutationCreateFollowsArgs = {
  from: Scalars['ID'];
  to: Scalars['ID'];
};

export enum Role {
  Anonymous = 'Anonymous',
  Admin = 'Admin',
  System = 'System',
  WebUser = 'WebUser'
}

export type Auth = {
  __typename: 'Auth';
  userId: Scalars['ID'];
  token: Scalars['String'];
  roles: Array<Role>;
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type LegacyStitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type NewStitchingResolver<TResult, TParent, TContext, TArgs> = {
  selectionSet: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type StitchingResolver<TResult, TParent, TContext, TArgs> = LegacyStitchingResolver<TResult, TParent, TContext, TArgs> | NewStitchingResolver<TResult, TParent, TContext, TArgs>;
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Glyph: ResolversTypes['Knows'] | ResolversTypes['Follows'] | ResolversTypes['User'];
  ID: ResolverTypeWrapper<Scalars['ID']>;
  StringMatchInput: StringMatchInput;
  String: ResolverTypeWrapper<Scalars['String']>;
  PageInput: PageInput;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  KnowsSubject: ResolversTypes['User'];
  KnowsObject: ResolversTypes['User'];
  KnowsSubjectQueryInput: KnowsSubjectQueryInput;
  KnowsObjectQueryInput: KnowsObjectQueryInput;
  KnowsQueryInput: KnowsQueryInput;
  Knows: ResolverTypeWrapper<Omit<Knows, '_subj' | '_obj'> & { _subj: Array<ResolversTypes['KnowsSubject']>, _obj: Array<ResolversTypes['KnowsObject']> }>;
  FollowsSubject: ResolversTypes['User'];
  FollowsObject: ResolversTypes['User'];
  FollowObjectQueryInput: FollowObjectQueryInput;
  FollowSubjectQueryInput: FollowSubjectQueryInput;
  FollowsQueryInput: FollowsQueryInput;
  Follows: ResolverTypeWrapper<Omit<Follows, '_subj' | '_obj'> & { _subj: Array<ResolversTypes['FollowsSubject']>, _obj: Array<ResolversTypes['FollowsObject']> }>;
  UserQueryInput: UserQueryInput;
  UserRelationQueryInput: UserRelationQueryInput;
  UserRelation: ResolversTypes['Knows'] | ResolversTypes['Follows'];
  User: ResolverTypeWrapper<Omit<User, '_rel'> & { _rel: Array<ResolversTypes['UserRelation']> }>;
  GraphQueryInput: GraphQueryInput;
  Query: ResolverTypeWrapper<RootValue>;
  CreateUserInput: CreateUserInput;
  Mutation: ResolverTypeWrapper<RootValue>;
  Role: Role;
  Auth: ResolverTypeWrapper<Auth>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Glyph: ResolversParentTypes['Knows'] | ResolversParentTypes['Follows'] | ResolversParentTypes['User'];
  ID: Scalars['ID'];
  StringMatchInput: StringMatchInput;
  String: Scalars['String'];
  PageInput: PageInput;
  Int: Scalars['Int'];
  KnowsSubject: ResolversParentTypes['User'];
  KnowsObject: ResolversParentTypes['User'];
  KnowsSubjectQueryInput: KnowsSubjectQueryInput;
  KnowsObjectQueryInput: KnowsObjectQueryInput;
  KnowsQueryInput: KnowsQueryInput;
  Knows: Omit<Knows, '_subj' | '_obj'> & { _subj: Array<ResolversParentTypes['KnowsSubject']>, _obj: Array<ResolversParentTypes['KnowsObject']> };
  FollowsSubject: ResolversParentTypes['User'];
  FollowsObject: ResolversParentTypes['User'];
  FollowObjectQueryInput: FollowObjectQueryInput;
  FollowSubjectQueryInput: FollowSubjectQueryInput;
  FollowsQueryInput: FollowsQueryInput;
  Follows: Omit<Follows, '_subj' | '_obj'> & { _subj: Array<ResolversParentTypes['FollowsSubject']>, _obj: Array<ResolversParentTypes['FollowsObject']> };
  UserQueryInput: UserQueryInput;
  UserRelationQueryInput: UserRelationQueryInput;
  UserRelation: ResolversParentTypes['Knows'] | ResolversParentTypes['Follows'];
  User: Omit<User, '_rel'> & { _rel: Array<ResolversParentTypes['UserRelation']> };
  GraphQueryInput: GraphQueryInput;
  Query: RootValue;
  CreateUserInput: CreateUserInput;
  Mutation: RootValue;
  Auth: Auth;
  Boolean: Scalars['Boolean'];
};

export type GlyphResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Glyph'] = ResolversParentTypes['Glyph']> = {
  __resolveType: TypeResolveFn<'Knows' | 'Follows' | 'User', ParentType, ContextType>;
  _id: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
};

export type KnowsSubjectResolvers<ContextType = Context, ParentType extends ResolversParentTypes['KnowsSubject'] = ResolversParentTypes['KnowsSubject']> = {
  __resolveType: TypeResolveFn<'User', ParentType, ContextType>;
};

export type KnowsObjectResolvers<ContextType = Context, ParentType extends ResolversParentTypes['KnowsObject'] = ResolversParentTypes['KnowsObject']> = {
  __resolveType: TypeResolveFn<'User', ParentType, ContextType>;
};

export type KnowsResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Knows'] = ResolversParentTypes['Knows']> = {
  _id: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  _subj: Resolver<Array<ResolversTypes['KnowsSubject']>, ParentType, ContextType, RequireFields<Knows_SubjArgs, never>>;
  _obj: Resolver<Array<ResolversTypes['KnowsObject']>, ParentType, ContextType, RequireFields<Knows_ObjArgs, never>>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type FollowsSubjectResolvers<ContextType = Context, ParentType extends ResolversParentTypes['FollowsSubject'] = ResolversParentTypes['FollowsSubject']> = {
  __resolveType: TypeResolveFn<'User', ParentType, ContextType>;
};

export type FollowsObjectResolvers<ContextType = Context, ParentType extends ResolversParentTypes['FollowsObject'] = ResolversParentTypes['FollowsObject']> = {
  __resolveType: TypeResolveFn<'User', ParentType, ContextType>;
};

export type FollowsResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Follows'] = ResolversParentTypes['Follows']> = {
  _id: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  _subj: Resolver<Array<ResolversTypes['FollowsSubject']>, ParentType, ContextType, RequireFields<Follows_SubjArgs, never>>;
  _obj: Resolver<Array<ResolversTypes['FollowsObject']>, ParentType, ContextType, RequireFields<Follows_ObjArgs, never>>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type UserRelationResolvers<ContextType = Context, ParentType extends ResolversParentTypes['UserRelation'] = ResolversParentTypes['UserRelation']> = {
  __resolveType: TypeResolveFn<'Knows' | 'Follows', ParentType, ContextType>;
};

export type UserResolvers<ContextType = Context, ParentType extends ResolversParentTypes['User'] = ResolversParentTypes['User']> = {
  _id: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  _rel: Resolver<Array<ResolversTypes['UserRelation']>, ParentType, ContextType, RequireFields<User_RelArgs, never>>;
  username: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  graph: Resolver<Array<ResolversTypes['Glyph']>, ParentType, ContextType, RequireFields<QueryGraphArgs, never>>;
};

export type MutationResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  createUser: Resolver<Maybe<ResolversTypes['User']>, ParentType, ContextType, RequireFields<MutationCreateUserArgs, 'user'>>;
  createKnows: Resolver<Maybe<ResolversTypes['Knows']>, ParentType, ContextType, RequireFields<MutationCreateKnowsArgs, 'from' | 'to'>>;
  createFollows: Resolver<Maybe<ResolversTypes['Follows']>, ParentType, ContextType, RequireFields<MutationCreateFollowsArgs, 'from' | 'to'>>;
};

export type AuthResolvers<ContextType = Context, ParentType extends ResolversParentTypes['Auth'] = ResolversParentTypes['Auth']> = {
  userId: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  token: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  roles: Resolver<Array<ResolversTypes['Role']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type Resolvers<ContextType = Context> = {
  Glyph: GlyphResolvers<ContextType>;
  KnowsSubject: KnowsSubjectResolvers<ContextType>;
  KnowsObject: KnowsObjectResolvers<ContextType>;
  Knows: KnowsResolvers<ContextType>;
  FollowsSubject: FollowsSubjectResolvers<ContextType>;
  FollowsObject: FollowsObjectResolvers<ContextType>;
  Follows: FollowsResolvers<ContextType>;
  UserRelation: UserRelationResolvers<ContextType>;
  User: UserResolvers<ContextType>;
  Query: QueryResolvers<ContextType>;
  Mutation: MutationResolvers<ContextType>;
  Auth: AuthResolvers<ContextType>;
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = Context> = Resolvers<ContextType>;
