import { ShallowEntity } from '../../gql-graph/types'
import * as Types from '../types'
export const User = (tag: string): ShallowEntity<Types.User> => ({
  __typename: 'User',
  _id: `_id[${tag}]`,
  username: `username[${tag}]`,
})
