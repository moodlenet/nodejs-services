import { Follows } from './Follows'
import { Knows } from './Knows'
import { User } from './User'

export type ShallowTypeMocks = typeof ShallowTypeMocks
export const ShallowTypeMocks = {
  User,
  Knows,
  Follows,
}
