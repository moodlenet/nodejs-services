import { ShallowRelation } from '../../gql-graph/types'
import * as Types from '../types'

export const Follows = (tag: string): ShallowRelation<Types.Follows> => ({
  __typename: 'Follows',
  _id: `_id[${tag}]`,
})
