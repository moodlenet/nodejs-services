import { ShallowRelation } from '../../gql-graph/types'
import * as Types from '../types'

export const Knows = (tag: string): ShallowRelation<Types.Knows> => ({
  __typename: 'Knows',
  _id: `_id[${tag}]`,
})
