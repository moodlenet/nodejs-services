import { GraphQLServerOptions } from 'apollo-server-core/src/graphqlOptions'
import { ApolloServer } from 'apollo-server-express'
import express from 'express'
import { graphql, GraphQLSchema } from 'graphql'
import { Context } from '../gql'
import { buildMongoPipeline } from '../gql-graph/mongo/buildMongoPipeline'
import { buildDocumentSelectionRoot } from '../gql-graph/documentSelection'
import { graphCollection } from '../mongo/collection'

type Cfg = {
  httpPort: number
  schema: GraphQLSchema
}
export const start = ({ httpPort, schema }: Cfg) => {
  const app = express()
  const apollo = new ApolloServer({ schema, executor })
  apollo.applyMiddleware({ app })
  return app.listen(httpPort)
}

const executor: GraphQLServerOptions['executor'] = async (requestContext) => {
  console.log('executor')
  const ctx: Context = {}
  const res = await graphql({ ...requestContext, contextValue: ctx })
  console.dir({ 'executor res': res }, { depth: 15 })
  console.dir({ 'executor $graph': ctx }, { depth: 15 })
  const documentSelection = buildDocumentSelectionRoot(ctx.$graph)
  console.dir({ 'executor documentSelection': documentSelection }, { depth: 15 })
  if (documentSelection) {
    const pipeline = buildMongoPipeline(documentSelection)
    console.log('executor pipeline', JSON.stringify(pipeline, null, 2))
    const coll = await graphCollection<any>()
    moreQueries(0, pipeline)
    const start = Number(new Date())
    const data = await coll.aggregate(JSON.parse(JSON.stringify(pipeline))).next()

    console.log('elapsed time', Number(new Date()) - start)

    return { data }
  }
  return Promise.resolve(res)
}

function moreQueries(amount: number, pipeline: any[]) {
  for (let i = 0; i < amount; i++) {
    graphCollection().then(async (coll) => {
      const curs = coll.aggregate([...pipeline, { $unwind: '$graph' }])
      //    .group({ _id: null, count: { $sum: 1 } })
      // console.log((
      await curs.toArray()
      // ).length)
      curs.close()
    })

    // .then(console.log)
  }
}
