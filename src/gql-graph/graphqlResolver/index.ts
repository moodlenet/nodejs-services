import { Context } from '../../gql'
import { ResolverFn } from '../../gql/types'
import { ShallowTypeMocks } from '../../gql/shallowTypes'
import { typeInfo, getParent } from './helpers'
import { GraphQueryObj, TraverseRelation } from '../types'

export function defaultGraphFieldResolver(): ResolverFn<any, any, Context, any> {
  return (parent, args, context, info) => {
    const { fieldName, parentType, returnType, schema } = info
    if (!context.$graph) {
      context.$graph = {
        qObj: {
          __typename: 'Query',
          alias: 'Query',
          directives: {},
          traverse: [],
          traverseRelation: null,
          fieldName: fieldName,
          select: [],
        },
      }
    }
    console.log(`defaultFieldResolver `, {
      parent,
      parentTypeName: parentType.name,
      fieldName,
      path: info.path,
      args,
    })
    const _typeInfo = typeInfo(schema, returnType)
    const __typenames = _typeInfo.objs.map((_) => _.name)
    const isTop = !info.path.prev

    const parentQ = getParent(info, context)
    console.log(`defaultFieldResolver `, {
      parentQ,
      _typeInfo,
    })
    if (!parentQ) {
      throw new Error('defaultFieldResolver no parentQ')
    }
    if (isTop || isTraverseRelation(fieldName)) {
      parentQ.traverse.push(
        ...__typenames.map<GraphQueryObj>((__typename) => ({
          __typename,
          alias: `${info.path.key}`,
          match: args?.query ? args?.query[__typename] : undefined,
          page: args?.page,
          select: [],
          traverse: [],
          traverseRelation: isTraverseRelation(fieldName) ? fieldName : null,
          directives: {},
          fieldName: fieldName,
        }))
      )

      const fieldReturn = __typenames.map((__typename) => {
        const type = (__typename as any) as keyof ShallowTypeMocks
        const obj = ShallowTypeMocks[type](`${info.path.key}:${fieldName} : ${type}`)
        console.log('*', type, obj)
        return obj
      })

      console.log(`fieldReturn ${fieldName}`, fieldReturn)

      return fieldReturn
    } else {
      if ('string' == typeof info.path.key) {
        parentQ.select.push({ fieldName: fieldName, alias: info.path.key })
      }
      return parent[fieldName]
    }
  }
}

const isTraverseRelation = (_: string): _ is TraverseRelation =>
  ['_subj', '_obj', '_rel'].includes(_)
