import { Path } from 'graphql/jsutils/Path'
import { Context } from '../../gql'
import {
  GraphQLResolveInfo,
  GraphQLOutputType,
  GraphQLObjectType,
  GraphQLScalarType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLUnionType,
  GraphQLInterfaceType,
  GraphQLSchema,
} from 'graphql'
import { GraphQueryObj } from '../types'

export const typeInfo = (
  schema: GraphQLSchema,
  gqlOutputType: GraphQLOutputType,
  isList = false,
  isNullable = true
): { objs: (GraphQLObjectType | GraphQLScalarType)[]; isList: boolean; isNullable: boolean } => {
  console.log('gqlOutputType', gqlOutputType)
  if (gqlOutputType instanceof GraphQLObjectType || gqlOutputType instanceof GraphQLScalarType) {
    return {
      objs: [gqlOutputType],
      isList,
      isNullable,
    }
  } else if (gqlOutputType instanceof GraphQLList) {
    return typeInfo(schema, gqlOutputType.ofType, true, isNullable)
  } else if (gqlOutputType instanceof GraphQLNonNull) {
    return typeInfo(schema, gqlOutputType.ofType, isList, false)
  } else if (gqlOutputType instanceof GraphQLUnionType) {
    const objs = gqlOutputType.getTypes()
    return {
      objs,
      isList,
      isNullable,
    }
  } else if (gqlOutputType instanceof GraphQLInterfaceType) {
    const objs = [...schema.getImplementations(gqlOutputType).objects]
    return {
      objs,
      isList,
      isNullable,
    }
  } else {
    console.error(gqlOutputType)
    throw new Error(`can't typeInfo on ${gqlOutputType.name}[${gqlOutputType.constructor.name}]`)
  }
}

export const getParent = (info: GraphQLResolveInfo, context: Context) => {
  if (!context.$graph) {
    return null
  }
  const rootgraphqueryobj: GraphQueryObj = context.$graph.qObj
  const revPath = reverseKeyPath(info.path)
  console.log(revPath)
  const found: GraphQueryObj | null | undefined = revPath.reduce<any>((curr, currPath) => {
    if (!curr) {
      return
    }
    if ('string' == typeof currPath.key) {
      const currQueryObj = curr as GraphQueryObj
      return currQueryObj.traverse.filter((q) => {
        return q.alias === currPath.key
      })
    } else {
      const currTraverse = curr as GraphQueryObj[]
      return currTraverse[currPath.key]
    }
  }, rootgraphqueryobj)

  return found
}

const reverseKeyPath = (_path: Path) => {
  let path: Path | undefined = _path
  const result: Path[] = []
  while ((path = path.prev)) {
    result.unshift(path)
  }
  return result
}
