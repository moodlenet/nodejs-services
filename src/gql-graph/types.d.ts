import { ObjectID } from 'mongodb'

export type ShallowEntity<T> = Omit<T, '_rel'>
export type ShallowRelation<T> = Omit<T, '_obj' | '_subj'>

export type GqlType = { _id: string; __typename: string }
export type GqlNode = GqlType & { _rel: any[] }
export type GqlRelation = GqlType & { _obj: any; _subj: any }

export type TraverseRelation = '_obj' | '_subj' | '_rel'

// GraphQuery

export type GraphQuery = {
  qObj: GraphQueryObj
}

export type GraphQueryObj = Selection & {
  __typename: string
  traverseRelation: TraverseRelation | null
  match?: any
  page?: {
    limit?: number
    after?: string | ObjectID
  }
  directives: Record<string, any>
  select: Selection[]
  traverse: GraphQueryObj[]
}

export type Selection = {
  fieldName: string
  alias: string
}
export type Match = any
export type Limit = any

// Graph DocumentSelection

export type DocumentSelection = {
  [alias: string]: Field
}

export type Field = UnionLookupField | ValueField

export type ValueField = Selection

export type UnionLookupField = ValueField & {
  page?: {
    limit?: number
    after?: string | ObjectID
  }
  traverseRelation: TraverseRelation | null
  lookups: FieldLookup[]
}

export type FieldLookup = {
  __typename: string
  match?: any
  select: DocumentSelection
}
