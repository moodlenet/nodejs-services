import {
  DocumentSelection,
  GraphQuery,
  GraphQueryObj,
  UnionLookupField,
  ValueField,
} from '../types'
import { isUnionLookupField } from './helpers'

export const buildDocumentSelectionRoot = (
  root: GraphQuery | null | undefined
): DocumentSelection | null | undefined => {
  return root && buildDocumentSelection(root.qObj)
}

export const buildDocumentSelection = (qObj: GraphQueryObj): DocumentSelection | null => {
  if (!(qObj.select.length || qObj.traverse.length)) {
    return null
  }
  const documentValueFieldsSelections = qObj.select.reduce((_documentSelection, selection) => {
    const valueField: ValueField = selection
    return {
      ..._documentSelection,
      [selection.alias]: valueField,
    }
  }, {} as DocumentSelection)
  const documentUnionLookupFieldSelections = qObj.traverse.reduce(
    (_documentSelection, qObjSelection) => {
      const _existingAlias = _documentSelection[qObjSelection.alias]
      if (_existingAlias && !isUnionLookupField(_existingAlias)) {
        throw aliasConflictError(qObjSelection, qObj)
      }
      const select = buildDocumentSelection(qObjSelection)
      if (!select) {
        return _documentSelection
      }

      const unionLookupField: UnionLookupField =
        _existingAlias ||
        (_documentSelection[qObjSelection.alias] = {
          page: qObjSelection.page,
          fieldName: qObjSelection.fieldName,
          alias: qObjSelection.alias,
          lookups: [],
          traverseRelation: qObjSelection.traverseRelation,
        })

      unionLookupField.lookups.push({
        __typename: qObjSelection.__typename,
        match: qObjSelection.match,
        select,
      })
      return {
        ..._documentSelection,
        [qObjSelection.alias]: unionLookupField,
      }
    },
    {} as DocumentSelection
  )

  return {
    ...documentValueFieldsSelections,
    ...documentUnionLookupFieldSelections,
  }
}

const aliasConflictError = (selection: GraphQueryObj, par: GraphQueryObj) => {
  console.error(`A ValueField can't have same alias[${selection.alias}] as a UnionLookupField`, par)
  return new Error(`A ValueField can't have same alias[${selection.alias}] as a UnionLookupField`)
}
