import { Field, UnionLookupField } from '../types'

export const isUnionLookupField = (_: Field): _ is UnionLookupField => !!(_ && 'lookups' in _)
