import { ObjectID } from 'mongodb'
import { GqlNode, GqlRelation, ShallowEntity, ShallowRelation } from '../types'
export type MongoNode<T extends GqlNode> = Omit<T, '_id' | '_rel'> & { _id: ObjectID }
export type MongoRelation<T extends GqlRelation> = Omit<T, '_id' | '_subj' | '_obj'> & {
  _id: ObjectID
  _obj: ObjectID
  _subj: ObjectID
}

export const toMongoNode = <Node extends GqlNode>(
  gql_node: ShallowEntity<Node> | Omit<ShallowEntity<Node>, '_id'>
): MongoNode<Node> => {
  const _id = '_id' in gql_node ? new ObjectID(gql_node._id) : new ObjectID()

  const mongo_node = Object.entries(gql_node).reduce(
    (constructing_mongo_node, [key, val]) => {
      if (['_id', '_rel'].includes(key)) {
        return constructing_mongo_node
      }
      return {
        ...constructing_mongo_node,
        [key]: val,
      }
    },
    {
      _id,
    } as MongoNode<Node>
  )

  return mongo_node
}

export const toMongoRelation = <Rel extends GqlRelation>(
  gql_node: ShallowRelation<Rel> | Omit<ShallowRelation<Rel>, '_id'>,
  obj: ObjectID | string,
  subj: ObjectID | string
): MongoRelation<Rel> => {
  const _id = '_id' in gql_node ? new ObjectID(gql_node._id) : new ObjectID()

  const mongo_rel = Object.entries(gql_node).reduce(
    (constructing_mongo_rel, [key, val]) => {
      if (['_id'].includes(key)) {
        return constructing_mongo_rel
      }
      return {
        ...constructing_mongo_rel,
        [key]: val,
      }
    },
    {
      _id,
      _obj: new ObjectID(obj),
      _subj: new ObjectID(subj),
    } as MongoRelation<Rel>
  )

  return mongo_rel
}
