import { isUnionLookupField } from '../documentSelection/helpers'
import { DocumentSelection } from '../types'
export const buildMongoPipeline = (docS: DocumentSelection, notTop?: boolean) => {
  const lookups = [] as any[]
  const project = (notTop ? { __typename: true } : { _id: false }) as Record<string, any>

  Object.entries(docS).forEach(([_alias, field]) => {
    if (isUnionLookupField(field)) {
      const unionLookupPipeline = [] as any[]
      let _let: any = undefined
      const mainLookup = {
        as: field.alias,
        from: 'Graph',
        pipeline: [],
      } as any
      if (field.traverseRelation) {
        if (field.traverseRelation === '_rel') {
          _let = { nodeId: '$_id' }

          notTop &&
            unionLookupPipeline.push({
              $match: {
                // $expr: { $eq: [`$${field.traverseRelation}`, '$$nodeId'] },
                $expr: { $eq: [`$_subj`, '$$nodeId'] },
              },
            })
        } else {
          _let = { edgeSide: `$${field.traverseRelation}` } //=== '_obj' ? '$_subj' : '$_obj'
          notTop && unionLookupPipeline.push({ $match: { $expr: { $eq: [`$_id`, '$$edgeSide'] } } })
        }
      }
      // unionLookupPipeline.push({ $sort: { __typename: 1, username: 1 } })
      field.lookups.forEach((fieldLookup, index) => {
        const $match = { $and: [{ __typename: fieldLookup.__typename }] as any[] }
        fieldLookup.match && $match.$and.push(renameMatchFields(fieldLookup.match))

        if (!index) {
          mainLookup.pipeline = [
            ...unionLookupPipeline,
            { $match },
            ...buildMongoPipeline(fieldLookup.select, true),
          ]
          _let && (mainLookup.let = _let)
          lookups.push({ $lookup: mainLookup })
        } else {
          mainLookup.pipeline.push({
            $unionWith: {
              coll: 'Graph',
              pipeline: [
                ...unionLookupPipeline,
                { $match },
                ...buildMongoPipeline(fieldLookup.select, true) /* [0].$lookup.pipeline */,
              ],
            },
          })
        }
      })
      project[field.alias] = true
      mainLookup.pipeline.push({ $limit: field.page?.limit || 10 })
    } else {
      project[field.alias] = field.alias === field.fieldName ? true : `$${field.fieldName}`
    }
  })
  const stages = [...lookups]
  if (Object.keys(project).length) {
    stages.push({ $project: project })
  }
  if (notTop) {
    return stages
  } else {
    return [{ $limit: 1 }, ...stages]
  }
}

function renameMatchFields(match: any): any {
  if (Array.isArray(match)) {
    return match.map((_) => renameMatchFields(_))
  } else if ('object' === typeof match) {
    return Object.entries(match).reduce((_, [key, val]) => {
      return {
        ..._,
        [key.replace(/^_/, '$')]: renameMatchFields(val),
      }
    }, {})
  } else {
    return match
  }
}
