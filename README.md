# Nodejs GraphQL server

## Test it

### with yarn

yarn install -D  
yarn build  
yarn start

### with npm

npm install -D  
npm run build  
npm start

### Start a MongoDB instance

Listening on default port 27017 on host  
you may want to use docker for simplicity :  
`docker run --name mongo-gql --network host -ti mongo`

### populate with some data

`node generate_data/populate.js 1000 20000 500`  
arguments :

1. User nodes amount to generate
2. random Knows|Follows edges between Users amount to generate
3. generation batch size

### issue queries

Open this nice [online graphql client](https://graphiql-online.com/)  
set endpoint to `http://localhost:4001/graphql`  
and issue a query

```graphql
query someQuery {
  user_knows_user_knows: graph {
    # for each user
    ... on User {
      name: username
      knows: _rel {
        # show his known
        ... on Knows {
          kid: _id
          this_user: _obj {
            ... on User {
              # users
              username
              that_follows: _rel {
                # and for each of them show their followed
                ... on Follows {
                  this_other_user: _obj {
                    # users
                    ... on User {
                      username
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  followers: graph {
    # for each Follow
    ... on Follows {
      _id
      subject: _subj {
        # show the following user
        ... on User {
          username
        }
      }
      target: _obj {
        # and the followed user
        ... on User {
          username
        }
      }
    }
  }
}
```
