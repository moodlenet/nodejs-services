const { ObjectID } = require("mongodb");

module.exports = [
  {
    "_id": new ObjectID("5f7dc3bf689ab4dc81f01dc3"),
    "__typename": "User",
    "username": "Alice"
  },
  {
    "_id": new ObjectID("5f7dc3bf689ab4dc81f01dc4"),
    "__typename": "User",
    "username": "Bob"
  },
  {
    "_id": new ObjectID("5f7dc3bf689ab4dc81f01dc5"),
    "__typename": "User",
    "username": "Charlie"
  },
  {
    "_id": new ObjectID("5f7dc3bf689ab4dc81f01dc6"),
    "__typename": "User",
    "username": "Donna"
  },
  {
    "_id": new ObjectID("5f7dc3bf689ab4dc81f01dc7"),
    "__typename": "User",
    "username": "Ernest"
  }
]
