const users = require('./users')
const { ObjectID, MongoClient } = require('mongodb')
const userIdByName = (username) => users.find((user) => user.username === username)._id
const relationsMap = [
  ['Alice', 'Knows', 'Bob,Charlie,Donna'],
  ['Alice', 'Follows', 'Donna,Ernest,Charlie'],
  ['Bob', 'Knows', 'Ernest,Donna,Alice'],
  ['Bob', 'Follows', ''],
  ['Charlie', 'Knows', 'Ernest,Alice'],
  ['Charlie', 'Follows', 'Bob'],
  ['Donna', 'Knows', 'Bob,Charlie,Ernest'],
  ['Donna', 'Follows', ''],
  ['Ernest', 'Knows', 'Bob,Alice'],
  ['Ernest', 'Follows', 'Donna,Alice,Charlie'],
]
const all_relations_docs = relationsMap.reduce((_all_relation_docs, [subj_username, relation, obj_usernames]) => {
  const _subj = userIdByName(subj_username)
  const row_relation_docs = obj_usernames.split(',').filter(Boolean).map(_obj_name => ({
    _id: new ObjectID(),
    _obj: userIdByName(_obj_name),
    _subj,
    __typename: relation

  }))
  return [..._all_relation_docs, ...row_relation_docs]
}, [])

const all_docs = [...users, ...all_relations_docs];
(async () => {

  const client = new MongoClient('mongodb://localhost:27017/mn')
  await client.connect()
  const collection = client.db().collection('Graph')
  try { await collection.drop() } catch { }
  const result = await collection.insertMany(all_docs)
  await client.close()
  console.log(result)
})()
