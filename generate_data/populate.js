const faker = require('faker')
const { MongoClient, ObjectID } = require('mongodb')
let nodeIds = []
console.log(process.argv)
const nNodes = Number(process.argv[2]) || 1000
const nEdges = Number(process.argv[3]) || 1000
const batchSize = Number(process.argv[4]) || 500

  ; (async () => {
    const client = new MongoClient('mongodb://localhost:27017/mn', { poolSize: 100, useUnifiedTopology: true })
    await client.connect()
    const collection = client.db().collection('Graph')
    try {
      await collection.drop()
    } catch { }

    console.log(`making nodes`)
    await nodes(collection, nNodes, batchSize)
    console.log(`nodes done, ${nodeIds.length}`)
    console.log(`making edges`)
    await edges(collection, nEdges, batchSize)
    console.log(`edges done`)
    console.log(`creating indexes`)
    await await collection.createIndexes([
      {
        key: { __typename: 1, _id: 1 },
        sparse: true,
        unique: true,
      },
      {
        key: { __typename: 1, username: 1 },
        partialFilterExpression: { __typename: { $eq: 'User' } },
        //sparse: true,
      },
      {
        key: { username: 1 },
        // partialFilterExpression: { __typename: { $eq: 'User' } },
        sparse: true,
      },
      {
        key: { __typename: 1, _obj: 1 },
        // partialFilterExpression: { $and: [{ _obj: { $exists: true } }] },
        sparse: true,
      },
      {
        key: { __typename: 1, _subj: 1 },
        // partialFilterExpression: { $and: [{ _subj: { $exists: true } }] },
        sparse: true,
      },
      {
        key: { __typename: 1, _subj: 1, _obj: 1 },
        sparse: true,
        // partialFilterExpression: { $and: [{ _subj: { $exists: true } }, { _obj: { $exists: true } }] },
        // unique: true,
      },
    ])
    console.log(`indexes done`)

    //try { await tempCollection.drop() } catch { }
    client.close()
  })()
/** @argument collection {import('mongodb').Collection}*/
async function nodes(collection, n_nodes, batch_size) {
  if (n_nodes < 1) {
    return
  }
  console.log(`${n_nodes} nodes to go`)
  const users = []
  while (users.length < batch_size) {
    users.push({
      __typename: 'User',
      username: faker.internet.userName() + '_' + Math.random().toString(36).substring(2),
    })
  }
  const res = await collection.insertMany(users)
  nodeIds = [...nodeIds, ...Object.values(res.insertedIds)]
  return new Promise((resolve, reject) => {

    setImmediate(() => resolve(nodes(collection, n_nodes - batch_size, batch_size)))
  })
}
/** @argument collection {import('mongodb').Collection}*/
async function edges(collection, n_edges, batch_size) {

  if (n_edges < 1) {
    return
  }
  console.log(`${n_edges} edges to go`)
  // const ids = await collection.aggregate(
  //   [{ $match: { __typename: 'User' } }, { $sample: { size: batch_size * 2 } }, { $project: { _id: 1 } }]
  // ).toArray()
  const ids = getRandom(nodeIds, batch_size * 2)
  const rels = []
  while (rels.length < batch_size) {
    rels.push({
      __typename: Math.random() <= 0.5 ? 'Knows' : 'Follows',
      _obj: new ObjectID(ids[rels.length * 2]),
      _subj: new ObjectID(ids[rels.length * 2 + 1]),
    })
  }
  await collection.insertMany(rels)
  return new Promise((resolve, reject) => {
    setImmediate(() => resolve(edges(collection, n_edges - batch_size, batch_size)))
  })
}

function getRandom(arr, n) {
  var result = new Array(n),
    len = arr.length,
    taken = new Array(len);
  if (n > len)
    throw new RangeError("getRandom: more elements taken than available");
  while (n--) {
    var x = Math.floor(Math.random() * len);
    result[n] = arr[x in taken ? taken[x] : x];
    taken[x] = --len in taken ? taken[len] : len;
  }
  return result;
}



/**
 {
  graph {
    ... on User {
      ...UserFrag
    }
  }
}

fragment UserFrag on User {
  username
  followsAndKnows: _rel {
    ... on Follows {
      ...FollowFrag
    }
    ... on Knows {
      _obj {
        ... on User {
          username
        }
      }
    }
  }
}

fragment FollowFrag on Follows {
  user: _obj {
    ... on User {
      username
      knows: _rel {
        ... on Knows {
          knows: _obj {
            ... on User {
              username
            }
          }
        }
      }
    }
  }
}

 */


/**
 {
 graph(query:{User:{_or:[{username:{_gt:"Y"}},{username:{_eq:"Aaliyah.Berge46"}}]}}) {
   ... on User {
     ...UserFrag
   }
 }
}

fragment UserFrag on User {
 username
 followsAndKnows: _rel {
   # ... on Follows {
   #   ...FollowFrag
   # }
   ... on Knows {
     _obj (query:{User:{username:{_lt:"B"}}}){
       ... on User {
         username
       }
     }
   }
 }
}

# fragment FollowFrag on Follows {
#   user: _obj {
#     ... on User {
#       username
#       knows: _rel {
#         ... on Knows {
#           knows: _obj {
#             ... on User {
#               username
#             }
#           }
#         }
#       }
#     }
#   }
# }

*/

/**
 {
  graph(query: {User: {username: {_gt: "0"}}}, page: {limit: 10}) {
    ... on User {
      ...UserFrag
    }
  }
}

fragment UserFrag on User {
  username
  Knows: _rel(page: {limit: 10}) {
    ... on Knows {
      user: _obj {
        ... on User {
          username
          FollowsAndKnows: _rel(page: {limit:40}) {
            ... on Follows {
              user: _obj {
                ... on User {
                  username
                }
              }
            }
            ... on Knows {
              user: _obj {
                ... on User {
                  username
                }
              }
            }
          }
        }
      }
    }
  }
}

*/